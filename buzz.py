import RPi.GPIO as GPIO
import time

def start_buzzer():
    GPIO.setmode(GPIO.BCM) 
    GPIO.setup(25, GPIO.OUT) #Using port 25 for Buzzer output
    p = GPIO.PWM(25, 200)
    p.start(1)
    
    while True:                       
        for i in range(100, 300, 50): #replicating the sound of a siren
            p.ChangeFrequency(i)
            time.sleep(1)
    GPIO.cleanup()
