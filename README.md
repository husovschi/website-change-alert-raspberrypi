# Website-Change-Alert-RaspberryPi

There are moments when we want to constantly check if a website has changed, but we dont have the whole day to sit up in front of our computers refreshing webpages. So I have built a short python script that lets you know if something has changed.

__Disclaimer__: This project is designed to run on an Raspberry Pi but with little change can run on any computer

### About

This program can notify you in this ways:

* Using a buzzer on pin 25
* Sending an SMS using twilio

### Installation

```bash
pip install twilio
pip install urllib
```
